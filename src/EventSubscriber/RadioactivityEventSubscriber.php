<?php

namespace Drupal\radioactivity_alter\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\radioactivity\Event\EnergyAlterEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\radioactivity_alter\RadioactivityAlterService;

/**
 * Class RadioactivityEventSubscriber
 * @package Drupal\radioactivity_alter\EventSubscriber
 */
class RadioactivityEventSubscriber implements EventSubscriberInterface {

  /**
   * @var RadioactivityAlterService
   */
  protected $radioactivityAlterSrv;
  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * RadioactivityEventSubscriber constructor.
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param RadioactivityAlterService $radioactivityAlterSrv
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RadioactivityAlterService $radioactivityAlterSrv) {
    $this->entityTypeManager = $entityTypeManager;
    $this->radioactivityAlterSrv = $radioactivityAlterSrv;
  }

  /**
   * @return array
   */
  public static function getSubscribedEvents() {
    $events = ['event_subscriber_radioactivity.alter_energy' => 'alterEnergy'];
    return $events;
  }

  /**
   * @param EnergyAlterEvent $event
   */
  public function alterEnergy(EnergyAlterEvent $event) {
    $energy = $event->getEnergy();
    $nid = $event->getNid();
    $new_energy = $this->radioactivityAlterSrv->checkAlterEnergy($energy, $nid);

    $event->setEnergy($new_energy);
  }

}

