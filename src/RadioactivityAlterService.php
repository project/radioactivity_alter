<?php

namespace Drupal\radioactivity_alter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class for reacting to radioactivity emit events.
 */
class RadioactivityAlterService {

  /**
   * The entity type manager.
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a RadioactivityAlter processor.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Recalculates the energy.
   *
   * @param int $energy
   *   The current energy
   *
   * @param int $nid
   *   The node id to which the energy is to be applied
   *
   * @return int $new_energy
   *   The recalculated energy
   */
  public function checkAlterEnergy(int $energy, int $nid) {
    $new_energy = $energy;
    $node = $this->entityTypeManager->getStorage("node")->load($nid);
    $field_name = $this->getAlterFieldName($node);
    if ($field_name == "") {
      return $new_energy;
    }

    $alter_factor = $node->get($field_name)->value;

    if (is_numeric($alter_factor) && $alter_factor >= 0 && $alter_factor <= 200) {
      $new_energy = $energy * ($alter_factor / 100);
    }
    return $new_energy;
  }

  /**
   * Tries to find the field name by field type.
   *
   * @param EntityInterface $node
   *   The node for which we need to find the field name
   *
   * @return string $field_name
   *   The field name, if found
   */
  private function getAlterFieldName(EntityInterface $node) {
    $field_name = "";
    foreach ($node->getFieldDefinitions() as $fieldDefinition) {
      // Try to find a Suppress Energy type of field.
      if ($fieldDefinition->getType() == "radioactivity_alter") {
        $field_name = $fieldDefinition->getName();
      }
    }
    return $field_name;
  }

}
