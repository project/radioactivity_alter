<?php

namespace Drupal\radioactivity_alter\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'radioactivity' field type.
 *
 * @FieldType(
 *   id = "radioactivity_alter",
 *   label = @Translation("Radioactivity Alter"),
 *   description = @Translation("Radioactivity energy Alter"),
 *   default_widget = "radioactivity_energy_alter",
 * )
 */
class RadioactivityAlterField extends FieldItemBase
{

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'unsigned' => true,
          'not null' => false
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('integer')
      ->setLabel(t('Alter'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === null || $value === '';
  }

}
