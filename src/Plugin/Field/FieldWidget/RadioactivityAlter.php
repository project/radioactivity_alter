<?php

namespace Drupal\radioactivity_alter\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'radioactivity_energy' widget.
 *
 * @FieldWidget(
 *   id = "radioactivity_energy_alter",
 *   label = @Translation("Energy Alter"),
 *   field_types = {
 *     "radioactivity_alter"
 *   }
 * )
 */
class RadioactivityAlter extends WidgetBase
{

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'suppress' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';

    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
    ];

    $element["#title"] = $this->t("Alter boost by %");
    $element["#description"] = $this->t("Allowed values are from 0 to 200. 0 - 99 will slow down energy respectively. 101-200 will speed up energy. <br> This is a percentage over the default energy as set in the radioactivity settings");

    if (!isset($form['advanced'])) {
      return ['suppress' => $element];
    }

    // Put the form element into the form's "advanced" group.
    return [
      '#type' => 'details',
      '#group' => 'advanced',
      '#title' => $this->t("Alter Radioactivity"),
      '#required' => TRUE,
      '#weight' => '40',
      'value' => $element,
    ];
  }

}
